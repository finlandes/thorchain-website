import { getPoolData } from '$lib/services/swap';
import axios from 'axios';

import { assets } from './constants';
import { formatNumber } from './helpers';
import { getRunePrice } from './rune';

export interface LPTableData {
	name: string;
	chain: string;
	liquidity: string;
	poolAPY: string;
	depositURL: string;
	color: string;
	icon: any;
}

export interface Stats {
	addLiquidityCount: string;
	addLiquidityVolume: string;
	dailyActiveUsers: string;
	impermanentLossProtectionPaid: string;
	monthlyActiveUsers: string;
	runeDepth: string;
	runePriceUSD: string;
	swapCount: string;
	swapCount24h: string;
	swapCount30d: string;
	swapVolume: string;
	switchedRune: string;
	synthBurnCount: string;
	synthMintCount: string;
	toAssetCount: string;
	toRuneCount: string;
	uniqueSwapperCount: string;
	withdrawCount: string;
	withdrawVolume: string;
}

export const getStats = async (): Promise<Stats> => {
	try {
		const { data } = await axios.get<Stats>('https://midgard.ninerealms.com/v2/stats');

		return data;
	} catch (error) {
		console.error(error);
	}
};

export const getRuneStats = async (): Promise<{
	runeDepth: number;
	runePriceUSD: number;
}> => {
	try {
		const response = await axios.get('https://midgard.ninerealms.com/v2/stats');
		return {
			runeDepth: +response.data.runeDepth,
			runePriceUSD: +response.data.runePriceUSD,
		};
	} catch (error) {
		console.error(error);
	}
};

export const getLPTableData = async (): Promise<LPTableData[]> => {
	const runePrice = await getRunePrice();

	const poolData = await getPoolData();

	const tableData: LPTableData[] = [];

	for (const asset of assets) {
		try {
			const assetData = poolData.find((data) => data.asset === asset.assetCode);
			const liquidity = ((Number(assetData.runeDepth) / 1e8) * +runePrice * 2).toString();
			const apy = assetData.poolAPY;

			tableData.push({
				name: asset.name,
				chain: asset.chain,
				liquidity: formatNumber(+liquidity),
				poolAPY: apy,
				depositURL: asset.depositURL,
				color: asset.color,
				icon: asset.icon,
			});
		} catch (error) {
			console.log(error);
		}
	}

	return tableData;
};

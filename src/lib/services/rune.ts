import axios from 'axios';

export const getRunePrice = async (): Promise<number> => {
	const { data } = await axios.get('https://midgard.ninerealms.com/v2/stats');

	return Number(data.runePriceUSD);
};

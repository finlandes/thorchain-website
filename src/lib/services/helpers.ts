import currency from 'currency.js';
import millify from 'millify';

export const convertToPercentage = (value: number | string, base: number): number => {
	return +((+value / base) * 100).toFixed(2);
};

export const convertToCurrency = (value: number): string => {
	return currency(value, { separator: ',' }).format();
};

export const formatNumber = function (value: number | string): string {
	return millify(+value,{precision:1});
};

export const formatNumberTwoDecimal = function (value: number | string): string {
	return millify(+value,{precision:2});
};
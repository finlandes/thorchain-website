# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte).

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
npm init svelte@next

# create a new project in my-app
npm init svelte@next my-app
```

> Note: the `@next` is temporary

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

When using the Google Cloud App Engine adapter, modify the generated `app.yaml` to include the following code block on each item. This gives the website an A+ security rating.

```yaml
    secure: always
    http_headers:
      Strict-Transport-Security: max-age=86400; includeSubDomains
      X-Frame-Options: DENY
      Content-Security-Policy: style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline'; font-src 'self' data:; img-src 'self' data:; default-src 'self' *.ninerealms.com *.coingecko.com
      X-Content-Type-Options: nosniff
      X-XSS-Protection: 1; mode=block
```
